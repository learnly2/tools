# External Tools / Services Configuration - README

This README file provides instructions on how to run rabbitMQ message broker and keycloak identity service.

# RabbitMQ
The best & simple way to run rabbitMQ is by using docker. 

In the rabbitMQ directory under the root deployment directory there is a docker compose file `docker-compose.yml`. 

Open the terminal and run `docker-compose.yml -d`. 

This will start a RabbitMQ container listening on the default port of 5672 default `guest` user and `guest` password.

The container will on run with configurations and port that reflect the backend services requirements.

# Keycloak

We will also run this using docker.

In the keycloak directory under the root deployment directory there is a docker compose file `docker-compose.yml`.

Open the terminal and run `docker-compose.yml -d`.

This will start a keycloak container listening on the default port of 8080 with default `admin` user and `admin` password.

To align with backend services requirements, the following configurations will need to be made:

##Creating a realm
1. Open the browser and got to `http://localhost:8080/`
2. Click on `Administration console`. This will redirect to a login page.
3. Populate the `username` & `password` fields with default `admin` username & `admin` password and click `Signin`.
4. On top left, click the dropdown option labeled `master` the click `Create Realm`.
5. On the page that appears, populate `Realm name` field with the name `pharmacy`. Make sure the `Enabled` option is selected and click `Create`. This will create the pharmacy realm and redirect pharmacy dashboard.

##Creating a client
1. On the left panel, click on `Clients` then `Create client`.
2. On the page that appears, populate the `Client ID` with `pharmacypoc` then click on `Next`. You can optionally provide and name and description.
3. On the next page, switch on the `Client authentication` option and on `Authentication flow` options, check `Service accounts roles` and make sure `Direct access grants` and `Standard flow` options are checked then click save.
4. At this point the client `pharmacypoc` is already created. Now we fine tune just 2 items.
5. Click on `Service account roles` tab then click on `Assign role` button. This will open a modal with filtering options labeled `Filter by realm roles` on the top left. Click and select `Filter by clients`. Look for `realm-managementmanage-users` role, check and click `Assign`.
6. The other item will be created after the next step.
7. Take note of the clientId `pharmacypoc` and client secret which is automatically generated in `Credentials` tab.
8. Use the client secret and clientId in `authClient` `application-dev.properties` keycloak configuration
```
###client
keycloak.client_id=pharmacypoc
keycloak.client_secret=[client_secret]
```

##Creating Roles & Groups
1. On the left panel, click on `Groups`.
2. On the page that appears, click on `Create group` button.A modal will appear, populate `Name` field with `PHARMACY_ADMIN` name and click create.
3. On the left panel, click on `Realm roles`.
4. On the page that appears, click on `Create role` button. A page will appear, populate `Role name` field with `ROLE_ADMIN` and optional description with text of your choice the click `Save`.
5. Go back to `Groups`, click on  `PHARMACY_ADMIN` group then `Role mapping` tab then click on `Assign role`. This will open a modal, Select previously created `ROLE_ADMIN` and click on `Assign`.
6. Finally, Go back to `Clients` Click on `Client scopes` tab the on the listed items click on the blue `pharmacypoc-dedicated` scope.
7. On the page that appears, click on `Scope` tab and turn off `Full scope allowed` switch. A new will `Assign role` button will appear click and on the modal, select `ROLE_ADMIN` and `realm-managementmanage-users` roles, check and click `Assign`.

##Creating a user
1. On the left panel, click on `Users` then `Add user`.
2. On the page that appears, populate the `Username` with `example@gmail.com` then switch on the `Email verified` option then at the bottom click on `Join Groups`. On the modal, select the `PHARMACY_ADMIN` group the click `Join`. You can optionally provide and Email, First name & Last name but leave `Required user actions` then click `Create`.
3. At this point the user `example@gmail.com` is already created.Now we just have to add credentials.
4. Click on `Credentials` tab then `Set password` button. On the modal populate `Password` and `Password confirmation` then switch off `Temporary` option and click `Save`. Confirm the Save password and click on `Save password` button.
5. Now the user is created with username `example@gmail.com` and password `previous step password`. Keep note.

##Keycloak endpoints
1. On the left panel, click on `Realm settings`, on the page that appears scroll down to `Endpoints` and click on `Endpoints
   OpenID Endpoint Configuration`.
2. Copy the `token_endpoint` and `end_session_endpoint` and update `authClient` application-dev.properties file
```
###token url
keycloak.token.url=http://0.0.0.0:8080/realms/pharmacy/protocol/openid-connect/token

###logout url
keycloak.logout.url=http://0.0.0.0:8080/realms/pharmacy/protocol/openid-connect/logout
```
3. Then copy `issuer` & `jwks_uri` and update `financeService`, `inventoryService`, `profileService` application-dev.properties file
```
###Keycloak
spring.security.oauth2.resourceserver.jwt.issuer-uri=http://0.0.0.0:8080/realms/pharmacy
spring.security.oauth2.resourceserver.jwt.jwk-set-uri=http://localhost:8080/realms/pharmacy/protocol/openid-connect/certs

```

# Architecture
The app is complex to configure / setup because I used the microservice architecture to achieve
1. Loosely coupling
2. Common domain language
3. Minimal services for quick deployment and scalability
4. Services breakdown when working with different teams (Separation of concerns) e.t.c

The authentication service is decoupled from the main services and standardized which means it can be used with other stacks too to provide uniform authentication model.

# Request Flow
ClientApp -----> Gateway ---->Backend Services ----> Database

Reverse the arrows for the response flow.

# Database
All services are using postgresql database and can be configured to use different DBMS. PostgreSQL has great performance benchmarks, great support community, open source and easy data replication when scaling and support
for `foreign data wrapper` for connecting with external datasource especially in microservice architecture.

## End ##
In case of any challenge please reach out
